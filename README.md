# iot-plant-nodemcu-esp8266
## pt-br
Este é um projeto de hardware destino acadêmico relacionado ao trabalho de conclusão de curso (**TCC**) de Sistemas de informação.
O tema do projeto aborda sustentabilidade, automação e aplicação web/app.
Automação de rega para manter a saúde de temperos e hortaliças.

Nele contém lógicas elaboradas para interagir com os componentes de: sensor de umidade; válvula solenóide; bomba d'água e "relé" (*relay*)

**Contém:**
- Criação de Web server local usado o módulo wifi incluso na placa ESP8266 NodeMCU.
- lógica de ativação e desativação do "relé" e componentes por request.
- lógica de cálculo da umidade para o sensor de umidade e o request pelo valor atual da umidade.

## en.
This is an academic target hardware project related to the coursework (** TCC **) of Information Systems.
The project's theme addresses sustainability, automation and web / app application.
Watering automation to maintain the health of spices and vegetables.

It contains logic designed to interact with the components of: humidity sensor; solenoid valve; water pump and "relay"

**Contains:**
- Creation of local web server using the wifi module included in the ESP8266 NodeMCU card.
- logic for activating and deactivating the "relay" and components by request.
- humidity calculation logic for the humidity sensor and the request for the current humidity value

https://github.com/esp8266/Arduino/issues/732#issuecomment-311164938
