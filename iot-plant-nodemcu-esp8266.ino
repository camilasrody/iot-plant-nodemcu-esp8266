// Webserver na WiFi network + sensor de umidade + liga/desliga relé
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiClient.h>

#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

#define WLAN_SSID     "sualan"
#define WLAN_PASSWORD "suasenha"

MDNSResponder mdns;
ESP8266WebServer server(80);

float humidity;
unsigned long previousMillis = 0;
const long interval = 2000;

int sensor = A0; // to receive data from Soil moisture sensor O/P pin */
int pinoFadeBomba = 04;
int pinoSolenoide = 05;

// Auxiliar variables to store the current output state
float statusBomba = 0;
float statusSolenoide;

int Number_of_Samples;

void setup() {
  Serial.begin(115200);
  Serial.print("\nIniciando...\n");
  Serial.println("ESP Chip Id: " + (String)ESP.getChipId());

  pinMode(sensor, INPUT);
  pinMode(pinoFadeBomba, OUTPUT);
  pinMode(pinoSolenoide, OUTPUT);

  Number_of_Samples = 1000;

  // Soft default OFF mod
  // digitalWrite(pinoFadeBomba, HIGH);
  // digitalWrite(pinoSolenoide, HIGH);
  // Serial.print("[!] Desligando Reles por padrão. \n");

  delay(10);

  humidity = FazLeituraUmidade();

  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.mode(WIFI_STA);
  WiFi.begin(WLAN_SSID, WLAN_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
   delay(500);
   Serial.print(".");
  }
  Serial.println();

  Serial.println("WiFi connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  if (mdns.begin("planta", WiFi.localIP())) {
    Serial.println("mDNS responder started");
  }


  // Register webserver routes
  server.onNotFound(handleNotFound);
  server.on("/", handleRoot);

  server.on("/toggle-1.json", []() {
    unsigned long currentMillis = millis();
    float brightnessPercent;

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      String newValue = server.arg("newValue");
      brightnessPercent = alternaPotencia(pinoFadeBomba, newValue.toFloat());
    }

    StaticJsonBuffer<500> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["current"] = brightnessPercent;

    String jsonString;
    root.printTo(jsonString);

    // Serial.println(jsonString);
    server.send(200, "application/json", jsonString);
  });

  server.on("/toggle-2.json", []() {
    unsigned long currentMillis = millis();
    float brightnessPercent;

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;
      String newValue = server.arg("newValue");
      brightnessPercent = alternaPotencia(pinoSolenoide, newValue.toFloat());
    }

    StaticJsonBuffer<500> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["current"] = brightnessPercent;

    String jsonString;
    root.printTo(jsonString);

    // Serial.println(jsonString);
    server.send(200, "application/json", jsonString);
  });
  
  server.on("/humidity", HTTP_GET, [](){
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;

      humidity = FazLeituraUmidade();

      if (isnan(humidity)) {
        Serial.println("Failed to read from sensor!");
        return;
      }
    }

    String webString = "Humiditiy " + String((int)humidity) + "%";
    Serial.println(webString);
    server.send(200, "text/plain", webString);
  });
  
  server.on("/humidity.json", [](){
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      previousMillis = currentMillis;

      humidity = FazLeituraUmidade();

      if (isnan(humidity)) {
        Serial.println("Failed to read from sensor!");
        return;
      }

              // Serial.println("Reporting " + String((int)humidity) + " % humidity");
    }

    StaticJsonBuffer<500> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    root["humidity"] = humidity;

    String jsonString;
    root.printTo(jsonString);

          // Serial.println(jsonString);
    server.send(200, "application/json", jsonString);
  });

  server.on("/info.json", [](){
    unsigned long currentMillis = millis();

    // if (currentMillis - previousMillis >= interval) {
    //     previousMillis = currentMillis;
    //     //rele = ( digitalRead(pin) == LOW )
    //     // Serial.println("Reporting " + String((int)humidity) + " % humidity");
    // }

    StaticJsonBuffer<500> jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();

    float currentValue = determineVolt(pinoFadeBomba);
    float currentPercent = ( 100 * currentValue ) / 1024;
    root["statusBomba"] = currentPercent;

    currentValue = determineVolt(pinoSolenoide);
    currentPercent = ( 100 * currentValue ) / 1024;
    root["statusSolenoide"] = currentPercent;

    String jsonString;
    root.printTo(jsonString);

    // Serial.println(jsonString);
    server.send(200, "application/json", jsonString);
  });

  server.begin();
  Serial.println("HTTP server started. Waiting for clients.");

}

void loop() {
  server.handleClient();
}


void handleRoot() {
  server.send(200, "text/plain", "Hello from esp8266.");
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

//Função: faz a leitura do nível de umidade
//Parâmetros: nenhum
//Retorno: umidade percentual (0-100)
//Observação: o ADC do NodeMCU permite até, no máximo, 3.3V. Dessa forma,
//            para 3.3V, obtem-se (empiricamente) 978 como leitura de ADC
float FazLeituraUmidade(void) {
  int ValorADC;
  float UmidadePercentual;
  // Quanto maior o numero lido do ADC, menor a umidade.
  // Sendo assim, calcula-se a porcentagem de umidade por:
  //
  //   Valor lido                 Umidade percentual
  //      _    0                           _ 100
  //      |                                |
  //      |                                |
  //      -   ValorADC                     - UmidadePercentual
  //      |                                |
  //      |                                |
  //     _|_  978                         _|_ 0
  //
  //   (UmidadePercentual-0) / (100-0)  =  (ValorADC - 978) / (-978)
  //      Logo:
  //      UmidadePercentual = 100 * ((978-ValorADC) / 978)

  ValorADC = analogRead(sensor);   //978 -> 3,3V
  // Serial.println("ADC = " + String((int)ValorADC));

  // Converte a variação do sensor de 0 a 1023 para 0 a 100
  UmidadePercentual = map(ValorADC, 1023, 0, 0, 100);
  // UmidadePercentual = 100 * ((1024-(float)ValorADC) / 1024);

  // Serial.println("Umidade Percentual = " + String((int)UmidadePercentual) + "%");
  return UmidadePercentual;
}

int determineVolt (int pin) {
  float sampleI = 0;
  float sumI = 0;
  for (unsigned int n = 0; n < Number_of_Samples; n++)
  {
    sampleI = analogRead(pin);
    sumI += sampleI;
  }
  return sumI / Number_of_Samples;
}

// how many points to fade the LED by
int fadeAmount = 5;

float alternaPotencia(int pin, float newValuePercent) {
  Serial.println("------------------------------");

  // Converte a variação do sensor de 0 a 1023 para 0 a 100
  float brightnessPercent = map(statusBomba, 0, 1023, 0, 100); // ( 100 * brightness ) / 1023;

  float newValue = ( 1023 * newValuePercent ) / 100;

  bool isMore = newValue > statusBomba;
  
  Serial.println( String((isMore) ? "⬆" : "⬇") + " [statusBomba] " + String((int)statusBomba) + " (" + String(brightnessPercent) + "%) to " + String(newValue) + " (" + String(newValuePercent) + "%)" );
  
  do {

    // Converte a variação do sensor de 0 a 1023 para 0 a 100
    brightnessPercent = map(statusBomba, 0, 1023, 0, 100);

    isMore = newValue > statusBomba;
    
    // change the brightness for next time through the loop:
    if ( isMore ) {
      statusBomba = statusBomba + fadeAmount;
    }else if(statusBomba == fadeAmount) {
      statusBomba = 0;
    }else{
      statusBomba = statusBomba - fadeAmount;
    }

    // Serial.println("[statusBomba] " + String(statusBomba) + " (" + String(brightnessPercent) + " % ) ");
    Serial.println( String((isMore) ? "⬆" : "⬇") + " [statusBomba] " + String((int)statusBomba) + " (" + String(brightnessPercent) + "%) to " + String(newValue) + " (" + String(newValuePercent) + "%)" );

    // how bright the pin is
    analogWrite(pin, statusBomba);   

    // wait for 30 milliseconds to see the dimming effect    
    delay(30);
  } while ( ! isMore && statusBomba > newValue || isMore && statusBomba < newValue );
  
  return brightnessPercent;
}

// bool alternaPotencia(int pin)
// {
//   int status = digitalRead(pin);
//   if ( status == LOW ) {
//     digitalWrite(pin, HIGH);
//     Serial.println("Pino #"+ String(pin) +" HIGH");
//     return true;
//   }
//   digitalWrite(pin, LOW);
//   Serial.println("Pino #"+ String(pin) +" LOW");
//   return false;
// }

// A short password seems to have unpredictable results so use one that's around 8 characters or more in length. The guidelines are that a wifi password must consist of 8 to 63 ASCII-encoded characters in the range of 32 to 126 (decimal)
